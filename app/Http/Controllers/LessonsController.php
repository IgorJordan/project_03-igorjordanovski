<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LessonRequest;
use App\Http\Requests\LessonEditRequest;
use App\Category;
use App\Lesson;
use App\Banner;

class LessonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $lessons = Lesson::where('category_id', $id)->paginate(10);
        $category = Category::where('id', $id)->first();
        $categories = Category::all();
        $banners = Banner::all();
        $counter = 1;
        
        return view('lessons', compact('lessons', 'category', 'banners', 'categories', 'counter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $category = Category::find($id);

        return view('add-lesson', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LessonRequest $request, $id)
    {
        $count = Lesson::where('category_id', '=', $id)->count();

        $lesson = new Lesson();
        $lesson->title = $request->title;
        $lesson->icon = $request->icon;
        $lesson->text = $request->text;
        $lesson->lesson_num = $count + 1;
        $lesson->category_id = $id;
        $lesson->save();

        return redirect()->route('lessons', ['id' => $id])->with('message', 'Успешно додадовте нова лекција');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson_id = Lesson::find($id);

        return view('edit-lesson', compact('lesson_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LessonEditRequest $request, $id)
    {
        $lesson = Lesson::find($id);
        if(!empty($lesson))
        {
            $lesson->title = $request->title;
            $lesson->icon = $request->icon;
            $lesson->text = $request->text;
            $lesson->lesson_num = $request->lesson_number;
            $lesson->save();
        } else {
            return redirect()->back();
        }

        return redirect()->route('lessons', ['id' => $lesson->category_id])->with('message', 'Успешно ја изменивте лекцијата');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson = Lesson::find($id);
        
        if(!empty($lesson))
        {
            $lesson->delete();
        }
        
        return redirect()->route('lessons', ['id' => $lesson->category_id])->with('message', 'Успешно ја избришавте лекцијата');
    }
}
