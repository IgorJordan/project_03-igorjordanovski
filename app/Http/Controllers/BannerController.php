<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BannerRequest;
use App\Banner;

class BannerController extends Controller
{
    public function index()
    {
        return view('banner');
    }

    public function set(BannerRequest $req)
    {
        $banner = Banner::where('id','=','1')->first();
        
        if (!empty($banner))
        {
            $banner->title = $req->bannerTitle;
            $banner->text = $req->bannerText;
            $banner->link = $req->link;
            $banner->save();
        } else {
            return redirect()->back();
        }

        return redirect()->route('lessons', ['id' => 1])->with('message', 'Успешно поставивте нов банер');
    }
}
