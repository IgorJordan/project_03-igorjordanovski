<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentInputRequest;
use App\Student;

class StudentController extends Controller
{
    public function store(StudentRequest $req)
    {
        $student = new Student();
        $student->email = $req->email;
        $student->category_id = $req->categoryLesson;
        $student->save();

        return redirect()->back()->with('message', 'Успешно се регистриравте');
    }

    public function noCategory(StudentInputRequest $req)
    {
        $student = new Student();
        $student->email = $req->emailCall;
        $student->save();

        return redirect()->route('category')->with('message', 'Успешно се регистриравте');
    }

    public function withCategory(StudentInputRequest $req, $id)
    {
        $student = new Student();
        $student->email = $req->emailCall;
        $student->category_id = $id;
        $student->save();

        return redirect()->back()->with('message', 'Успешно се регистриравте со категоријата од моменталната страна');
    }
}
