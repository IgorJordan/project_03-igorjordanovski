<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Student;

class AdminController extends Controller
{
    public function show()
    {
        return view('admin');
    }

    public function panel()
    {
        $students = Student::paginate(15);
        $counter = 1;

        return view('admin-panel', compact('students', 'counter'));
    }
    
    public function destroy($id)
    {
        $student = Student::find($id);

        if(!empty($student))
        {
            $student->delete();
        }
        
        return redirect()->route('panel')->with('message', 'Успешно го избришавте студентот');
    }
}
