<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentInputRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emailCall' => 'required|max:255|email',
        ];
    }

    public function messages()
    {
        return [
            'emailCall.required' => 'Пополнете ја Вашата електронска адреса',
            'emailCall.max:255' => 'Електронската адреса мора да има максимум 255 карактери',
            'emailCall.email' => 'Пополнивте невалидна електронска адреса',
        ];
    }
}
