<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:60',
            'icon' => 'required|max:60',
            'text' => 'required|max:400',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Напишете наслов на лекцијата',
            'title.max:60' => 'Насловот мора да има максимум 60 карактери',
            'icon.required' => 'Пополнете икона',
            'icon.max:60' => 'Патот до иконата мора да има максимум 60 карактери',
            'text.required' => 'Напишете текст на лекцијата',
            'text.max:400' => 'Текстот на лекцијата мора да има максимум 400 карактери',
        ];
    }
}
