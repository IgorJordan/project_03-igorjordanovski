<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoryName' => 'required|max:30',
            'description' => 'required|max:240',
            'image' => 'required|max:240',
        ];
    }

    public function messages()
    {
        return [
            'categoryName.required' => 'Напишете име на категоријата',
            'categoryName.max:30' => 'Името мора да има максимум 30 карактери',
            'description.required' => 'Дадете опис на категоријата',
            'description.max:240' => 'Описот на категоријата мора да има максимум 240 карактери',
            'image.required' => 'Поставете слика',
            'image.max:240' => 'Патот до сликата мора да има максимум 240 карактери',
        ];
    }
}
