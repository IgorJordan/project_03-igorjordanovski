<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:255|email',
            'categoryLesson' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Пополнете ја Вашата електронска адреса',
            'email.max:255' => 'Електронската адреса мора да има максимум 255 карактери',
            'email.email' => 'Пополнивте невалидна електронска адреса',
            'categoryLesson.required' => 'Изберете категорија',
        ];
    }
}
