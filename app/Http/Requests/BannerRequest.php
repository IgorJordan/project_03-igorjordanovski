<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bannerTitle' => 'required|max:30',
            'bannerText' => 'required|max:300',
            'link' => 'required|max:200|url',
        ];
    }

    public function messages()
    {
        return [
            'bannerTitle.required' => 'Напишете наслов на банерот',
            'bannerTitle.max:30' => 'Насловот мора да има максимум 30 карактери',
            'bannerText.required' => 'Напишете текст на банерот',
            'bannerText.max:300' => 'Текстот на банерот мора да има максимум 240 карактери',
            'link.required' => 'Поставете линк',
            'link.max:200' => 'Патот до линкот мора да има максимум 200 карактери',
            'link.max:url' => 'URL-то не е валидно',
        ];
    }
}
