<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function categoryName() 
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    protected $table = 'students';
    protected $fillable = ['email', 'categoryName'];
}
