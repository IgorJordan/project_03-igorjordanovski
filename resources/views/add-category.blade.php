@extends('layouts.project')

@section('content')
<div class="container add-margin">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="{{ route('category') }}">Назад</a></li>
                <li role="presentation" class="active"><a class="active" href="">Додади категорија</a></li>
            </ul>
        </div>
    </div>
    <section class="bg-white">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Додади нова категорија:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 project-form">
            <form action="{{ route('storeCategory') }}" method="POST">
                @csrf

                @if (count($errors) > 0)
                    <div class="alert alert-danger text-center" id="errorDiv">
                        @foreach ($errors->all() as $error)
                            <p><i class="fas fa-exclamation-circle"></i> {{ $error }}</p>
                        @endforeach
                    </div>
                @elseif (session('message'))
                    <div class="alert alert-success text-center" id="errorDiv">
                        <p><i class="fas fa-check-circle"></i> {{session("message")}}</p>
                    </div>
                @endif

                <div class="form-group">
                    <label for="categoryName">Име на категоријата</label>
                    <input type="text" name="categoryName" class="form-control" id="catName">
                </div>
                <div class="form-group">
                    <label for="image">Слика</label>
                    <input type="text" name="image" class="form-control" id="image" placeholder="">
                </div>
                <div class="form-group">
                    <label for="description">Опис</label>
                    <textarea name="description" class="form-control" id="description" rows="2"></textarea>
                </div>

                <button type="submit" class="btn btn-block btn-default btn-color"><b>Додај</b></button>
              </form>
        </div>
    </div>
    </section>
</div>

@endsection