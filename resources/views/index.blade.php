
@extends('layouts.project')

@section('content')
<div class="container content">
    <div class="row control-buttons">
        @guest
        
        @else
        <div class="col-md-12">
            <a class="btn btn-primary" href="{{ route('addCategory') }}"><b>Додади</b></a><span><b> нова категорија</b></span>
        </div>
        @endguest
        
            

    </div>
    <div class="row">
        @foreach ($categories as $category)
        <div class="col-md-4 col-sm-6">
            <a class="card-a" href="{{ route('lessons', ['id' => $category->id]) }}">
                <div class="card">
                    <img class="img-responsive card-img" src="{{ $category->image }}" alt="card image">
                    <h3 class="card-margin">{{ $category->name }}</h3>
                    <p class="card-margin descr">{{ $category->description }}</p>
                    <p class="text-center footer-card">Лекции</p>
                </div>
            </a>

            <div class="btn-group-justified control-buttons">
                @guest
                
                @else
                <a class="btn btn-info" href="{{ route('editCategory', ['id' => $category->id]) }}"><i class="fas fa-edit"></i><b> Измени</b></a>
                <a class="btn btn-danger" href="{{ route('deleteCategory', ['id' => $category->id]) }}"><i class="fas fa-trash-alt"></i><b> Избриши</b></a>
                @endguest
                
            </div>
        </div>
        @endforeach
    </div>
    {{$categories->links()}}
</div>

@endsection
        

        