@extends('layouts.project')

@section('content')
    <div class="container content">
        @guest
            
        @else
        <div class="row control-buttons">
            <div class="col-md-8 col-sm-8 col-xs-6 btn-font">
                <a class="btn btn-primary" href="{{ route('addLesson', ['id' => $category->id]) }}"><b>Додади</b></a><span><b> нова лекција</b></span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6 btn-font">
                <a class="btn btn-primary" href="{{ route('banner') }}"><b>Постави</b></a><span><b> нов банер</b></span>
            </div>
        </div>
        @endguest
        
        <div class="row test">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    
                    @foreach ($lessons as $lesson)
                    <div class="col-md-12 baner">
                        <div class="row"> 
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <a class="lesson-link" href="#"><p><span>#{{ $counter++ }} </span><b> {{$lesson->title}} </b></p></a>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4 text-right">
                                <p class="date-size"><i class="{{$lesson->icon}}"></i><span class="date-span"> {{ $lesson->created_at->format('Y-m-d') }}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="lesson-text">{{$lesson->text}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="btn-group-justified control-buttons">
                        @guest
                            
                        @else
                        <a class="btn btn-info" href="{{ route('editLesson', ['id' => $lesson->id]) }}"><i class="fas fa-edit"></i><b> Измени</b></a>
                        <a class="btn btn-danger" href="{{ route('deleteLesson', ['id' => $lesson->id]) }}"><i class="fas fa-trash-alt"></i><b> Избриши</b></a>
                        @endguest
    
                    </div>
                    @endforeach

                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="baner">

                    @foreach ($banners as $banner)
                    <h3><b>{{ $banner->title }}</b></h3>
                    <p>{{ $banner->text }}</p>
                    
                    <div class="btn-more">
                        <a href="{{ $banner->link }}" target="_blank"><p class="more"><b>Повеќе</b><span><i class="fas fa-arrow-right"></i></span></p></a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        {{$lessons->links()}}
    </div>
@endsection