@include('layouts.head')
        <header>
            <nav class="navbar navbar-default" role="navigation">
               
            @if (Request::is('brainster/admin'))
                <div class="navbar-header nav-change" style="width: 100%;">
                    <a class="" href="{{ route('category') }}"><img class="logo img-responsive nav-admin" src="{{ asset('img/brainster_logo.png') }}" alt="brainster_logo"></a>
                </div>
            </nav>
        </header>
            @elseif (Request::is('brainster/add-lesson/*') || Request::is('brainster/add-category') 
            || Request::is('brainster/edit-category/*') || Request::is('brainster/edit-lesson/*') 
            || Request::is('brainster/set-new-banner') || Request::is('brainster/admin-panel'))

            <div class="navbar-header nav-change">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('category') }}"><img class="logo img-responsive" src="{{ asset('img/brainster_logo.png') }}" alt="brainster_logo"></a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav ml-auto navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @else
                        <li class="nav-item dropdown btnLi">
                            <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Request::is('brainster/admin-panel'))

                                @else
                                <li><a href="{{ route('panel') }}">Листа на студенти</a></li>
                                <li role="separator" class="divider"></li>
                                @endif
                                <li><a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    Одјави се
                                </a></li>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>

            @else
                <div class="navbar-header nav-change">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <a class="navbar-brand" href="{{ route('category') }}"><img class="logo img-responsive" src="{{ asset('img/brainster_logo.png') }}" alt="brainster_logo"></a>
                </div>
            
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Академии</a></li>
                        <li><a href="#">Вебинари</a></li>
                        <li><a href="#">Тест за кариера</a></li>
                        <li><a href="#">Блог</a></li>
                       
                        @guest
                            <li class="btnLi">
                                <a class="btn-custom" href="#" data-toggle="modal" data-target="#loginModal">Приклучи се</a>
                            </li>
                        @else
                            <li class="nav-item dropdown btnLi">
                                <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Request::is('brainster/admin-panel'))

                                    @else
                                    <li><a href="{{ route('panel') }}">Листа на студенти</a></li>
                                    <li role="separator" class="divider"></li>
                                    @endif
                                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Одјави се
                                    </a></li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </ul>
                            </li>
                        @endguest
                        
                    </ul>

                </div>
            </nav>
        </header>
        
        @include('layouts.modal')

        <div class="container">
            @if (count($errors) > 0)
                <div class="alert alert-danger text-center" id="errorDiv">
                    @foreach ($errors->all() as $error)
                        <p><i class="fas fa-exclamation-circle"></i> {{ $error }}</p>
                    @endforeach
                </div>
            @elseif (session('message'))
                <div class="alert alert-success text-center" id="errorDiv">
                    <p><i class="fas fa-check-circle"></i> {{session("message")}}</p>
                </div>
            @endif
        </div>

        <div class="container text-center bg-purple purple">
            <h2>Приклучи се на 1350 ентузијасти и учи дигитални вештини.</h2>
            <h2>Бесплатно.</h2>
            @if (Request::is('/'))
            <form action="{{ route('callStudent') }}" method="post">
            @else
            <form action="{{ route('callStudent2', ['id' => $category->id]) }}" method="post">
            @endif
                @csrf
                
                <div class="input-group input-new">
                    <input type="email" name="emailCall" class="form-control input-color" placeholder="E-mail Address">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Пријави се</button>
                    </span>
                </div>
            </form>
             <p>Можеш да се исклучиш од листата на меилови во секое време</p>
        </div>

        @endif