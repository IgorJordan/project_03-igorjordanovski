<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        @if (Request::is('brainster/admin'))
        <title>Brainster | ADMIN LOGIN PAGE</title>
        @elseif (Request::is('brainster/add-lesson/*'))
        <title>Brainster | Додади нова лекција</title>
        @elseif (Request::is('brainster/add-category'))
        <title>Brainster | Додади нова категорија</title>
        @elseif (Request::is('brainster/edit-category/*'))
        <title>Brainster | Измени категорија</title>
        @elseif (Request::is('brainster/edit-lesson/*'))
        <title>Brainster | Измени лекција</title>
        @elseif (Request::is('brainster/set-new-banner'))
        <title>Brainster | Постави нов банер</title>
        @elseif (Request::is('/'))
        <title>Brainster | Категории на лекции</title>
        @elseif (Request::is('brainster/lessons/*'))
        <title>Brainster | {{$category->name}} - лекции</title>
        @elseif (Request::is('brainster/admin-panel'))
        <title>Brainster | Админ панел</title>
        @endif

        <!-- css -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    </head>
    <body>
