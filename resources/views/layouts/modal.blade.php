<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header purple">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Пријави се и избери ја својата омилена категорија</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('newStudent') }}" method="POST">
                @csrf
                
                <div class="form-group">
                    <label for="email">Електронска адреса</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="categoryLesson">Категорија</label>
                    <select class="form-control select-color" id="categoryLesson" name="categoryLesson">
                        <option value="">-- Избери категорија --</option>
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="modal-footer purple">
                    <button type="submit" class="btn btn-default" id="save">Зачувај</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
            </div>
        </div>
  
    </div>
</div>