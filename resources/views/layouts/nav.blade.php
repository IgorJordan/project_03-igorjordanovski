<header>
    <nav class="navbar navbar-default" role="navigation">
       
    @if (Request::is('brainster/admin'))
        <div class="navbar-header nav-change" style="width: 100%;">
            <a class="" href="{{ route('category') }}"><img class="logo img-responsive nav-admin" src="{{ asset('img/brainster_logo.png') }}" alt="brainster_logo"></a>
        </div>
    </nav>
</header>
    @elseif (Request::is('brainster/add-lesson/*') || Request::is('brainster/add-category') 
    || Request::is('brainster/edit-category/*') || Request::is('brainster/edit-lesson/*') 
    || Request::is('brainster/set-new-banner'))

    <div class="navbar-header nav-change">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('category') }}"><img class="logo img-responsive" src="{{ asset('img/brainster_logo.png') }}" alt="brainster_logo"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav ml-auto navbar-right">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>
</header>

    @else
        <div class="navbar-header nav-change">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        <a class="navbar-brand" href="{{ route('category') }}"><img class="logo img-responsive" src="{{ asset('img/brainster_logo.png') }}" alt="brainster_logo"></a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Академии</a></li>
                <li><a href="#">Вебинари</a></li>
                <li><a href="#">Тест за кариера</a></li>
                <li><a href="#">Блог</a></li>
                @guest
                    <li class="btnLi"><a class="btn-custom" href="#" data-toggle="modal" data-target="#loginModal">Приклучи се</a></li>
                @else
                    <li class="nav-item dropdown btnLi">
                        <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
                
            </ul>

        </div>
    </nav>
</header>
@endif