<footer id="footer">
    <div class="container-fluid text-center footer">
        <p><i class="far fa-copyright"></i> Copyright Brainster 2020, all rights reserved</p>
    </div>
</footer>

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
    //dynamical select option
    $(document).ready(function () {
        setTimeout(function() {
            $('#errorDiv').fadeOut('fast');
        }, 5000); // <-- time in milliseconds
    });
</script>
    
</body>
</html>