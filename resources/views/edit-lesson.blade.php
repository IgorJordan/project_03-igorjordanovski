@extends('layouts.project')

@section('content')
<div class="container except-margin">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="{{ URL::previous() }}">Назад</a></li>
                <li role="presentation" class="active"><a class="active" href="#">Измени лекција</a></li>
            </ul>
        </div>
    </div>
    <section class="bg-white">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Измени ја лекцијата:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 project-form">
            <form action="{{ route('updateLesson', ['id' => $lesson_id->id]) }}" method="POST">
                @csrf
                
                @if (count($errors) > 0)
                    <div class="alert alert-danger text-center" id="errorDiv">
                        @foreach ($errors->all() as $error)
                            <p><i class="fas fa-exclamation-circle"></i> {{ $error }}</p>
                        @endforeach
                    </div>
                @elseif (session('message'))
                    <div class="alert alert-success text-center" id="errorDiv">
                        <p><i class="fas fa-check-circle"></i> {{session("message")}}</p>
                    </div>
                @endif
                
                <div class="form-group">
                    <label for="lesson_number">Број на лекција</label>
                    <input type="text" name="lesson_number" class="form-control" id="lessonNum" value="{{$lesson_id->lesson_num}}">
                </div>
                <div class="form-group">
                    <label for="title">Наслов</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{$lesson_id->title}}">
                </div>
                <div class="form-group">
                    <label for="icon">Икона</label>
                    <input type="text" name="icon" class="form-control" id="icon" placeholder="" value="{{$lesson_id->icon}}">
                </div>
                <div class="form-group">
                    <label for="text">Текст</label>
                    <textarea name="text" class="form-control" id="text" rows="2">{{$lesson_id->text}}</textarea>
                </div>

                <button type="submit" class="btn btn-block btn-default btn-color"><b>Измени</b></button>
              </form>
        </div>
    </div>
    </section>
</div>

@endsection