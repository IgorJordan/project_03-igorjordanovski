@extends('layouts.project')

@section('content')
<div class="container add-margin">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="{{ URL::previous() }}">Назад</a></li>
                <li role="presentation" class="active"><a class="active" href="#">Измени банер</a></li>
            </ul>
        </div>
    </div>
    <section class="bg-white">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Постави нов банер:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 project-form">
            <form action="{{ route('setBanner') }}" method="POST">
                @csrf
                
                @if (count($errors) > 0)
                    <div class="alert alert-danger text-center" id="errorDiv">
                        @foreach ($errors->all() as $error)
                            <p><i class="fas fa-exclamation-circle"></i> {{ $error }}</p>
                        @endforeach
                    </div>
                @elseif (session('message'))
                    <div class="alert alert-success text-center" id="errorDiv">
                        <p><i class="fas fa-check-circle"></i> {{session("message")}}</p>
                    </div>
                @endif
                
                <div class="form-group">
                    <label for="bannerTitle">Банер наслов</label>
                    <input type="text" name="bannerTitle" class="form-control" id="bannTitle"">
                </div>
                <div class="form-group">
                    <label for="bannerText">Банер текст</label>
                    <textarea name="bannerText" class="form-control" id="bannText" rows="2"></textarea>
                </div>
                <div class="form-group">
                    <label for="link">Линк</label>
                    <input type="text" name="link" class="form-control" id="link" placeholder="">
                </div>

                <button type="submit" class="btn btn-block btn-default btn-color"><b>Постави</b></button>
              </form>
        </div>
    </div>
    </section>
</div>

@endsection