@extends('layouts.project')

@section('content')
<div class="container admin-panel">

    @if (session('message'))
    <div class="alert alert-success text-center" id="errorDiv">
        <p><i class="fas fa-check-circle"></i> {{session("message")}}</p>
    </div>
    @endif

    <div class="row justify-content-center" style="margin-top:50px">
        <div class="col-md-12 ">
            <table class="table table-hover">
                <thead>
                <tr class="table-header">
                    <th>#</th>
                    <th>Електронска адреса</th>
                    <th>Категорија</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student )
                    <tr>
                        <td>{{ $counter++ }}</td>
                        <td>{{ $student->email }}</td>
                        <td>{{ $student->categoryName->name ?? ''}}</td>
                        <td class="button text-center">
                            <a href="{{ route('deleteStudent', ['id' => $student->id]) }}" class="btn btn-md btn-danger">Избриши</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{$students->links()}}
    </div>
</div>

@endsection