<?php

use App\Http\Controllers\MainController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'CategoryController@index')->name('category');

Route::get('brainster/lessons/{id}', 'LessonsController@index')->name('lessons');

Route::post('brainster/new-student', 'StudentController@store')->name('newStudent');
Route::post('brainster/new-student-no-category', 'StudentController@noCategory')->name('callStudent');
Route::post('brainster/new-student-category/{id}', 'StudentController@withCategory')->name('callStudent2');

Route::get('brainster/admin', 'AdminController@show')->middleware('guest');

Route::middleware(['role:admin'])->group(function() {
    Route::get('brainster/add-category', 'CategoryController@create')->name('addCategory');
    Route::post('brainster/add-category', 'CategoryController@store')->name('storeCategory');
    Route::get('brainster/edit-category/{id}', 'CategoryController@edit')->name('editCategory');
    Route::post('brainster/edit-category/{id}', 'CategoryController@update')->name('updateCategory');
    Route::get('brainster/delete-category/{id}', 'CategoryController@destroy')->name('deleteCategory');

    Route::get('brainster/add-lesson/{id}', 'LessonsController@create')->name('addLesson');
    Route::post('brainster/add-lesson/{id}', 'LessonsController@store')->name('storeLesson');
    Route::get('brainster/edit-lesson/{id}', 'LessonsController@edit')->name('editLesson');
    Route::post('brainster/edit-lesson/{id}', 'LessonsController@update')->name('updateLesson');
    Route::get('brainster/delete-lesson/{id}', 'LessonsController@destroy')->name('deleteLesson');

    Route::get('brainster/set-new-banner', 'BannerController@index')->name('banner');
    Route::post('brainster/set-new-banner', 'BannerController@set')->name('setBanner');

    Route::get('brainster/admin-panel', 'AdminController@panel')->name('panel');
    Route::get('brainster/delete-student/{id}', 'AdminController@destroy')->name('deleteStudent');
});
