<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LessonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lessons')->delete();
        DB::table('lessons')->insert([

            [
                'title' => 'Avoid The Top 10 Mistakes Made By Beginning MARKETING',
                'text' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
                'icon' => 'fas fa-funnel-dollar',
                'lesson_num' => '1',
                'category_id' => '1',
                'created_at' => '2014-06-26 04:07:31',
            ],
            [
                'title' => 'Wondering How To Make Your MARKETING Rock? Read This!',
                'text' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
                'icon' => 'fas fa-mail-bulk',
                'lesson_num' => '2',
                'category_id' => '1',
                'created_at' => '2014-07-26 04:07:31',
            ],
            [
                'title' => '5 Brilliant Ways To Teach Your Audience About MARKETING',
                'text' => 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure.',
                'icon' => 'fas fa-envelope-open-text',
                'lesson_num' => '3',
                'category_id' => '1',
                'created_at' => '2015-06-26 04:07:31',
            ],
            [
                'title' => 'Find Out Now, What Should You Do For Fast MARKETING?',
                'text' => 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica.',
                'icon' => 'fas fa-ad',
                'lesson_num' => '4',
                'category_id' => '1',
                'created_at' => '2016-06-26 04:07:31',
            ],
            [
                'title' => '5 Things To Do Immediately About DESIGN',
                'text' => 'The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.',
                'icon' => 'fas fa-ruler-combined',
                'lesson_num' => '1',
                'category_id' => '2',
                'created_at' => '2014-06-27 04:07:31',
            ],
            [
                'title' => 'How I Improved My DESIGN In One Day',
                'text' => 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.',
                'icon' => 'fas fa-drafting-compass',
                'lesson_num' => '2',
                'category_id' => '2',
                'created_at' => '2014-06-29 04:07:31',
            ],
            [
                'title' => 'Here Is What You Should Do For Your DESIGN',
                'text' => 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.',
                'icon' => 'fas fa-swatchbook',
                'lesson_num' => '3',
                'category_id' => '2',
                'created_at' => '2017-06-26 04:07:31',
            ],
            [
                'title' => 'Believe In Your DESIGN Skills But Never Stop Improving',
                'text' => 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.',
                'icon' => 'fas fa-palette',
                'lesson_num' => '4',
                'category_id' => '2',
                'created_at' => '2018-06-26 04:07:31',
            ],
            [
                'title' => '3 Tips About UX/UI You Cannot Afford To Miss',
                'text' => 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk.',
                'icon' => 'fab fa-quinscape',
                'lesson_num' => '1',
                'category_id' => '3',
                'created_at' => '2014-05-26 04:07:31',
            ],
            [
                'title' => 'What You Should Have Asked Your Teachers About UX/UI',
                'text' => 'Nunc vulputate turpis sit amet nulla molestie semper. Fusce diam orci, tempus vitae mollis et, gravida eu augue. Curabitur id purus ultricies libero ultricies consectetur. Praesent ac consectetur lacus, non bibendum elit. Curabitur tempor turpis eget nisl tempus ullamcorper. Suspendisse vel mauris at tortor rutrum bibendum. Proin vitae mollis erat. Quisque vel feugiat urna, sed imperdiet magna.',
                'icon' => 'fab fa-acquisitions-incorporated',
                'lesson_num' => '2',
                'category_id' => '3',
                'created_at' => '2014-05-29 04:07:31',
            ],
            [
                'title' => 'Apply These 5 Secret Techniques To Improve UX/UI',
                'text' => 'Nam sed orci sodales, cursus nunc at, ornare purus. Donec fermentum laoreet sapien vitae molestie. Aliquam semper orci ut turpis lacinia tempus. Suspendisse sed dolor id turpis mollis dapibus. Donec luctus convallis ante iaculis consequat. Phasellus dictum urna nisi, a vulputate tellus sollicitudin condimentum. Suspendisse iaculis odio eget erat maximus, quis blandit neque iaculis.',
                'icon' => 'fas fa-eraser',
                'lesson_num' => '3',
                'category_id' => '3',
                'created_at' => '2014-06-28 04:07:31',
            ],
            [
                'title' => 'Find Out Now, What Should You Do For Fast DATA SCIENCE?',
                'text' => 'Pellentesque cursus lorem et dolor tempus, vitae facilisis lorem gravida. Nulla quis euismod metus. Nunc vestibulum, velit vestibulum tempor rhoncus, arcu enim fermentum lacus, quis hendrerit sapien odio sed odio. Proin id consectetur libero. Sed feugiat tellus massa, porttitor luctus tortor interdum sed.',
                'icon' => 'fas fa-database',
                'lesson_num' => '1',
                'category_id' => '4',
                'created_at' => '2019-06-26 04:07:31',
            ],
            [
                'title' => 'Apply These 5 Secret Techniques To Improve DATA SCIENCE',
                'text' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut rhoncus viverra finibus. Proin at porttitor nisl, sed euismod magna. Sed dictum eros enim, ac consectetur nisl vehicula dignissim. Maecenas non arcu et magna ullamcorper laoreet vitae ac orci.',
                'icon' => 'fas fa-atom',
                'lesson_num' => '2',
                'category_id' => '4',
                'created_at' => '2019-06-29 04:07:31',
            ],
            [
                'title' => 'DATA SCIENCE Is Bound To Make An Impact In Your Business',
                'text' => 'Donec accumsan dui ac sem ultrices ultricies. Nulla viverra eros at finibus volutpat. Vestibulum consectetur suscipit sem in dignissim. Fusce sem velit, consectetur at suscipit sed, blandit ac magna. Ut lacinia vehicula magna eget tincidunt. Vivamus id odio enim.',
                'icon' => 'fas fa-satellite-dish',
                'lesson_num' => '3',
                'category_id' => '4',
                'created_at' => '2019-07-26 04:07:31',
            ],
            [
                'title' => 'What Is WEB PROGRAMMING and How Does It Work?',
                'text' => 'Integer vel ipsum nulla. Proin fermentum mi tristique, eleifend est vel, cursus arcu. Vivamus ultricies arcu ut leo suscipit pharetra. Fusce enim purus, fermentum ut dolor vitae, iaculis tempor dolor. Mauris a orci nisl. Ut sem lectus, feugiat at tortor id, dictum blandit risus.',
                'icon' => 'fas fa-network-wired',
                'lesson_num' => '1',
                'category_id' => '5',
                'created_at' => '2014-03-26 04:07:31',
            ],
            [
                'title' => 'Get Better WEB PROGRAMMING Results By Following These Steps',
                'text' => 'Quisque in lobortis nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.',
                'icon' => 'fas fa-compress-arrows-alt',
                'lesson_num' => '2',
                'category_id' => '5',
                'created_at' => '2014-04-26 04:07:31',
            ],
            [
                'title' => '10 Best Practices For WEB PROGRAMMING',
                'text' => 'Aliquam gravida, dui vel lacinia volutpat, libero ligula elementum dui, eu faucibus leo metus id elit. Sed ante dui, elementum non nibh at, malesuada pretium justo. Morbi odio tortor, consectetur a accumsan sit amet, condimentum non eros.',
                'icon' => 'fas fa-laptop',
                'lesson_num' => '3',
                'category_id' => '5',
                'created_at' => '2016-02-26 04:07:31',
            ],
            [
                'title' => 'Here Is A Method That Is Helping WEB PROGRAMMING',
                'text' => 'Nam cursus fermentum dictum. Etiam a interdum elit. Cras egestas dictum sapien, ac lobortis velit malesuada at. Sed vel velit ullamcorper, ultrices ligula nec, dignissim mauris. Sed non eros sagittis, imperdiet arcu quis, faucibus justo. Aliquam erat volutpat. Pellentesque non quam lectus.',
                'icon' => 'fas fa-th-list',
                'lesson_num' => '4',
                'category_id' => '5',
                'created_at' => '2017-01-15 04:07:31',
            ],
            [
                'title' => 'BUSINESS Strategies For Beginners',
                'text' => 'Duis lacinia maximus gravida. Nullam faucibus non urna at viverra. Curabitur pulvinar auctor nibh, vel ultricies arcu. Integer a augue tempor, scelerisque nulla nec, tincidunt nisi. In accumsan pellentesque urna, lacinia fringilla nunc consectetur nec.',
                'icon' => 'fas fa-business-time',
                'lesson_num' => '1',
                'category_id' => '6',
                'created_at' => '2016-12-26 04:07:31',
            ],
            [
                'title' => 'Why You Never See BUSINESS That Actually Works',
                'text' => 'Aliquam eu interdum nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi metus lacus, interdum vitae feugiat vitae, auctor sit amet nisl. Nulla porta arcu eu feugiat cursus. Mauris ut pharetra est, eu cursus felis. Suspendisse venenatis non risus sit amet finibus.',
                'icon' => 'fas fa-user-tie',
                'lesson_num' => '2',
                'category_id' => '6',
                'created_at' => '2017-01-20 04:07:31',
            ],
            [
                'title' => 'What Alberto Savoia Can Teach You About BUSINESS',
                'text' => 'Vestibulum maximus tempor lorem ac ullamcorper. Sed vehicula nibh ut dui rhoncus congue. Duis hendrerit venenatis purus, et sodales sem vestibulum sit amet. In sem erat, suscipit a dignissim nec, facilisis sit amet purus. Vivamus egestas, nibh sit amet aliquam ornare, nunc tortor egestas leo, vitae sagittis sapien velit ut tellus.',
                'icon' => 'fas fa-briefcase',
                'lesson_num' => '3',
                'category_id' => '6',
                'created_at' => '2017-11-11 04:07:31',
            ]
        ]);

    }
}
