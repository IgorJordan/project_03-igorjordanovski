<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->delete();
        DB::table('banners')->insert([
            'title' => 'Избери ја својата иднина',
            'text' => 'Совладај ги најбараните вештини на денешницата преку практична работа. Поврзи се со сегашните и идните врвни професионалци. Отвори си...',
            'link' => 'https://brainster.co/'
        ]);
    }
}
